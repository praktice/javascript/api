'use strict';

const SetupEndpoint = require('./setup/');

module.exports = SetupEndpoint({
    name: 'auth',
    urls: [
        {
            requests: [{
                method: 'POST',
                response: { status: 'ok' }
            }]
        }
    ]
});
