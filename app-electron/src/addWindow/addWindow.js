const electron = require("electron");
// Send item to main.js
const { ipcRenderer } = electron;

const form = document.querySelector("form");
form.addEventListener("submit", submitForm);

function submitForm(e) {
  e.preventDefault();
  const item = document.querySelector("#item").value;
  // customName, Payload
  ipcRenderer.send("item:add", item);
}
