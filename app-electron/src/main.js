const electron = require("electron");
const url = require("url");
const path = require("path");

const { app, BrowserWindow, Menu, ipcMain } = electron;

let mainWindow;
let addWindow;
const isMac = process.platform == "darwin";
const isProduction = process.env.NODE_ENV != "prodiction";

// Catch Item Add
ipcMain.on("item:add", (evt, item) => {
  // Send to Main Window (comes from addWindow)
  mainWindow.webContents.send("item:add", item);
  addWindow.close();
});

// Listen for App
app.on("ready", () => {
  mainWindow = new BrowserWindow({});
  // Load HTML into mainWindow
  mainWindow.loadURL(
    // file://__dirname/mainwindow.html
    url.format({
      pathname: path.join(__dirname, "mainWindow.html"),
      protocol: "file:",
      slashes: true
    })
  );

  // Quit App When Closed (Any extra windows)
  mainWindow.on("closed", () => {
    app.quit();
  });

  // Build menu from Template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
  // Insert Menu
  Menu.setApplicationMenu(mainMenu);
});

// Handle create add window
function createAddWindow() {
  addWindow = new BrowserWindow({
    width: 300,
    height: 200,
    title: "Add Item"
  });
  // Load HTML into mainWindow
  addWindow.loadURL(
    // file://__dirname/mainwindow.html
    url.format({
      pathname: path.join(__dirname, "addWindow/addWindow.html"),
      protocol: "file:",
      slashes: true
    })
  );
  // Garbage Collection
  addWindow.on("close", () => {
    addWindow = null;
  });
}

// Menu Template
const mainMenuTemplate = [
  {
    label: "File",
    submenu: [
      {
        label: "Add Item",
        accelerator: isMac ? "Command+N" : "Ctrl+N",
        click() {
          createAddWindow();
        }
      },
      {
        label: "Clear Items",
        click() {
          mainWindow.webContets.send("item:clear");
        }
      },
      {
        role: "separator"
      },
      {
        label: "Quit",
        accelerator: isMac ? "Command+Q" : "Ctrl+Q",
        click() {
          app.quit();
        }
      }
    ]
  }
];

// If Mac, add empty object
if (isMac) {
  // Mac needs an empty item at the start, but no other OS
  mainMenuTemplate.unshift({});
}

// Add Developer Tools item if not in production
if (process.env.NODE_ENV != "prodiction") {
  mainMenuTemplate.push({
    label: "Developer Tools",
    submenu: [
      {
        label: "Toggle DevTools",
        accelerator: isMac ? "Command+I" : "Ctrl+I",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        }
      },
      {
        role: "reload"
      }
    ]
  });
}
