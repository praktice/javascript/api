const express = require('express');
const router = express.Router();

router.get('/', function (req, res, next) {
  res.render('dashboard/index', {});
});

router.get('/profile', function (req, res, next) {
  res.render('dashboard/profile', {});
});

module.exports = router;
