const webpack = require('webpack');
const path = require('path');
module.exports = {
  mode: 'development',
  devtool: 'eval-source-map',
  entry: {
    index: [
      'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
      './src/index.js'
    ]
  },
  output: {
    path: path.resolve(__dirname, './public'),
    filename: '[name].bundle.js',
    publicPath: '/',
    hotUpdateChunkFilename: '.hot/[id].[hash].hot-update.js',
    hotUpdateMainFilename: '.hot/[hash].hot-update.json'
  },
  plugins: [new webpack.HotModuleReplacementPlugin()]
};
