const { GraphQLServer } = require('graphql-yoga')
// 1
const typeDefs = `
type Query {
  info: String!
}
`

// 2
const resolvers = {
  Query: {
    info: () => String!,
  }
}

type Query {
  users: [User!]!
  user(id: ID!): User
}

type Mutation {
  createUser(name: String!): User!
}

type User {
  id: ID!
  name: String!
}

// 3
const server = new GraphQLServer({
  typeDefs,
  resolvers,
})

query {
  users {
    id
    name
  }
}

# Query a single user by their id
query {
  user(id: "user-1") {
    id
    name
  }
}

# Create a new user
mutation {
  createUser(name: "Bob") {
    id
    name
  }
}

server.start(() => console.log(`Server is running on http://localhost:4000`))
