import tokenService from './services/tokenService';
import { token } from './routes/token';
import { home } from './routes/home';

module.exports = {
  tokenService,
  token,
  home
};
