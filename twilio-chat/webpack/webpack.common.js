import path from 'path';
const PATHS = require('./webpack/includes/paths');
const PLUGINS = require('./webpack/includes/plugins');
const RULES = require('./webpack/includes/rules');

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    polyfills: './include/polyfills.js',
    app: `${PATHS.SRC}/index.js`
  },
  output: {
    filename: '[name].bundle.js',
    path: PATHS.OUTPUT
  },
  publicPath: PATHS.PUBLIC,
  optimization: {
    usedExports: true
  },
  module: {
    plugins: [
      // PLUGINS.CLEAN,
      PLUGINS.HTML,
      PLUGINS.HMR,
      PLUGINS.CACHE,
      PLUGINS.CACHE_PARALLEL
    ],
    rules: [
      RULES.STYLES,
      RULES.IMAGES,
      RULES.FONTS
    ]
  }
};
