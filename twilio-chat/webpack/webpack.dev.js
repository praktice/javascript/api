const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const PATHS = require('./include/paths');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: PATHS.OUTPUT,
    hot: true
  }
});
