const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const PLUGINS = require('./include/plugins');

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    PLUGINS.MINICSS
  ]
});
