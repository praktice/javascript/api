const path = require('path');
const PATHS = require('./include/paths');
const RULES = {};

/**
 * Styles
 */
RULES.STYLES = {
  test: /\.(s|a|c|le)ss$/,
  include: path.resolve(__dirname, PATHS.SRC), // Faster Loading
  use: [
    'style-loader',
    'css-loader',
    'sass-loader',
    'less-loader'
  ]
};

/**
 * Scripts
 */
RULES.SCRIPTS = {
  test: /\.js$/,
  include: path.resolve(__dirname, PATHS.SRC), // Faster Loading
  loader: 'babel-loader'
};

/**
* Images
*/
RULES.IMAGES = {
  test: /\.(png|svg|jpg|gif)$/,
  include: path.resolve(__dirname, PATHS.SRC), // Faster Loading
  use: [
    'file-loader'
  ]
};

/**
 * Fonts
 */
RULES.FONTS = {
  test: /\.(woff|woff2|eot|ttf|otf)$/,
  include: path.resolve(__dirname, PATHS.SRC), // Faster Loading
  use: [
    'file-loader'
  ]
};

module.exports = RULES;
