const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const PATHS = require('./includes/paths');

const PLUGINS = {};

PLUGINS.CLEAN = new CleanWebpackPlugin([
  PATHS.OUTPUT
]);

PLUGINS.HTML = new HtmlWebpackPlugin({
  title: 'Output Management'
});

PLUGINS.HOT = new webpack.HotModuleReplacementPlugin();

PLUGINS.CACHE = new HardSourceWebpackPlugin({
  cacheDirectory: 'node_modules/.cache/hard-source/[confighash]',
  configHash: function (webpackConfig) {
    return require('node-object-hash')({ sort: false }).hash(webpackConfig);
  },
  environmentHash: {
    root: PATHS.ROOT,
    directories: [],
    files: [ 'package-lock.json', 'yarn.lock' ]
  },
  cachePrune: {
    maxAge: 2 * 24 * 60 * 60 * 1000,
    sizeThreshold: 50 * 1024 * 1024
  }
});

PLUGINS.CACHE_PARALLEL = new HardSourceWebpackPlugin.ParallelModulePlugin({
  // How to launch the extra processes. Default:
  fork: (fork, compiler, webpackBin) => fork(
    webpackBin(),
    [ '--config', __filename ], {
      silent: true
    }
  ),
  // Number of workers to spawn. Default:
  numWorkers: () => require('os').cpus().length,
  // Number of modules built before launching parallel building. Default:
  minModules: 10
});

PLUGINS.MINICSS = new MiniCssExtractPlugin();

module.exports = PLUGINS;
