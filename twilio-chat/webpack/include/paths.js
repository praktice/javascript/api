const path = require('path');

const PATHS = {};
PATHS.ROOT = path.resolve(__dirname, '../../');
PATHS.SRC = path.resolve(PATHS.ROOT, '/src');
PATHS.OUTPUT = path.resolve(PATHS.ROOT, '/public');
PATHS.PUBLIC = '/';

module.exports = {
  PATHS
};
